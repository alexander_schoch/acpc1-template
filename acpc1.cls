\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{ppac}[2022/09/08 LaTeX class for ACPC1 reports by Alexander Schoch]

% Based on article class
\LoadClass[11pt]{article}

\RequirePackage{graphicx}
\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}
\RequirePackage{fancyhdr}
\RequirePackage{multicol}
\RequirePackage[version=4]{mhchem}
\RequirePackage{xurl}
\RequirePackage{hyperref}
\RequirePackage{float}
\RequirePackage{tcolorbox}
\RequirePackage{caption}
\RequirePackage[separate-uncertainty=true]{siunitx}
\RequirePackage[top=1in, bottom=1in, right=1in, left=1in]{geometry}
\RequirePackage{newfloat}
\RequirePackage{listings}
\RequirePackage{chemfig}
\RequirePackage{sectsty}
\RequirePackage{chngcntr}
\RequirePackage{subcaption}
\RequirePackage{pdfpages}
\RequirePackage{booktabs}
\RequirePackage{longtable}
\RequirePackage[leftline=false, rightline=false,innerleftmargin=0, innerrightmargin=0]{mdframed}
\RequirePackage{ifthen}

% set figure nubmering to section.number
%\counterwithin{figure}{section}

\linespread{1.25}
\renewcommand{\arraystretch}{1.25}

% Redefine titles
\allsectionsfont{\bfseries\sffamily}

% Defining Commands for maketitle
\newcommand{\thetitle}{change this with \texttt{\textbackslash title\{\}}}
\renewcommand{\title}[1]{
  \renewcommand{\thetitle}{#1}
}

\newcommand{\theauthorOne}{change this with \texttt{\textbackslash authorOne\{name\}\{e-mail\}}}
\newcommand{\theauthorOnemail}{}
\newcommand{\authorOne}[2]{
  \renewcommand{\theauthorOne}{#1}
  \renewcommand{\theauthorOnemail}{#2}
}
\newcommand{\theauthorTwo}{change this with \texttt{\textbackslash authorTwo\{name\}\{e-mail\}}}
\newcommand{\theauthorTwomail}{}
\newcommand{\authorTwo}[2]{
  \renewcommand{\theauthorTwo}{#1}
  \renewcommand{\theauthorTwomail}{#2}
}
\newcommand{\theauthorThree}{NONE}
\newcommand{\theauthorThreemail}{NONE}
\newcommand{\authorThree}[2]{
  \renewcommand{\theauthorThree}{#1}
  \renewcommand{\theauthorThreemail}{#2}
}

\newcommand{\theassistant}{change this with \texttt{\textbackslash
  assistant\{\}}}
\newcommand{\assistant}[1]{
  \renewcommand{\theassistant}{#1}
}

\newcommand{\theabstract}{change this with \texttt{\textbackslash abstract\{\}}}
\renewcommand{\abstract}[1]{
  \renewcommand{\theabstract}{#1}
}

\newcommand{\theplace}{change this with \texttt{\textbackslash place\{\}}}
\newcommand{\place}[1]{
  \renewcommand{\theplace}{#1}
}

\newcommand{\theexperiment}{change this with \texttt{\textbackslash
  experiment\{\}}}
\newcommand{\experiment}[1]{
  \renewcommand{\theexperiment}{#1}
}


% header/footer settings
\fancypagestyle{acpc1}{%
  \renewcommand{\headrulewidth}{0pt}
  \fancyhf{}
  \fancyhead[L]{Praktikum ACPC1, ETH Zürich, HS\,22}
  \fancyhead[R]{\theexperiment}
  \fancyfoot[C]{\thepage}
}
\pagestyle{acpc1}


% define maketitle command
\renewcommand{\maketitle}{
  \thispagestyle{plain}
  \vspace*{.5cm}
  \begin{center}
    {\huge\bfseries\sffamily\thetitle}\par\bigskip
    \vspace{.5cm}
    \begin{tabular}{rl}
      \theauthorOne & \href{mailto:\theauthorOnemail}{\theauthorOnemail} \\
      \theauthorTwo & \href{mailto:\theauthorTwomail}{\theauthorTwomail} \\
      \ifthenelse{\equal{\theauthorThree}{NONE}}{}{
        \theauthorThree & \href{mailto:\theauthorThreemail}{\theauthorThreemail} \\
      }
    \end{tabular}\par\bigskip
    \vspace{.5cm}
    Assistant: \theassistant
  \end{center}\par\bigskip
  \rule{\linewidth}{1pt}
    \textbf{\sffamily Abstract}\quad \theabstract\par
  \rule{\linewidth}{1pt}

  \vfill

  \ifthenelse{\equal{\theauthorThree}{NONE}}{
    % two signatures
    \parbox{\textwidth}{
      \centering \theplace, \today\\
      \vspace{2cm}
      \parbox{7cm}{
        \centering
        \rule{6cm}{1pt}\\
        \theauthorOne 
      }
      \hfill
      \parbox{7cm}{
        \centering
        \rule{6cm}{1pt}\\
        \theauthorTwo
      }
      \vspace{2cm}
    }
  }{
    % three signatures
    \parbox{\textwidth}{
      \centering \theplace, \today\\
      \vspace{2cm}
      \parbox{5cm}{
        \centering
        \rule{4.5cm}{1pt}\\
        \theauthorOne 
      }
      \hfill
      \parbox{5cm}{
        \centering
        \rule{4.5cm}{1pt}\\
        \theauthorTwo
      }
      \hfill
      \parbox{5cm}{
        \centering
        \rule{4.5cm}{1pt}\\
        \theauthorThree
      }
      \vspace{2cm}
    }
  }
  \newpage
}

% change SIrange command
\sisetup{mode=text,range-phrase = {\linebreak[0] $-$ \nolinebreak}}

% no indent
\parindent0mm
\setlength{\headheight}{25.2232pt}

% Chemfig settings for it to look kinda like chemdraw, but better
\setchemfig{atom sep=1.785em}
\setchemfig{bond offset=0.18265em}

% Definition of "scheme" environment
\DeclareFloatingEnvironment[
    fileext=los,
    listname={List of Schemes},
    name=Scheme,
    placement=tbhp,
    %within=section,
]{scheme}

% Code listings
\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstset{ 
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}; should come as last argument
  basicstyle=\footnotesize\ttfamily,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=single,	                   % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=R,                      % the language of the code
  morekeywords={*,...},            % if you want to add more keywords to the set
  numbers=left,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=1,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,                       % sets default tabsize to 2 spaces
}
